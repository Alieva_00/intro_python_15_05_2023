# a = 10
# b = 20
# c = a + b
#
# print(c)
#
# b = "20"
#
# c = a + b
#
# variable = 10
# Variable = 20
#
# variable1 = 10
# variable_2 = 10
# # 1variable2 = 10
#
# variable2 = 10
# # !variable2 = 10
# # %variable2 = 10
# # varia-ble2 = 10
# # variable 2 = 10
#
# _variable = 10
# __variable = 10
#
#
# # students_counter = 0
#
# # O o l I


# numbers

# int
#
# my_int = 30
#
# print(type(my_int))
#
# my_int2 = 13
#
# print(my_int2)
#
# result = my_int + my_int2
# print(result)
#
# result = my_int - my_int2
# print(result)
#
# result = my_int * my_int2
# print(result)
#
# result = my_int / my_int2
# print(result)
#
# result = my_int // my_int2
# print(result)
#
# result = my_int % my_int2  # 30 - 13 * 2
# print(result)
#
# result = my_int + (my_int2 - 3) * 6
# print(result)
#
# result = my_int ** 6
# print(result)
#
# my_int += my_int2  # my_int = my_int + my_int2
# my_int -= my_int2  # my_int = my_int - my_int2
#
#
# result = 100 ** 100
# print(result)

# float

# my_float1 = 1.5
# my_float2 = .5  # 0.5
#
# res = my_float1 + my_float2
# print(res)
#
# res = 1.5 * 2
# print(res)
# print(type(res))
#
# # res = my_float1 / 0
#
# print(res)
#
# my_float1 = 0.000000000000001
# print(my_float1)
#
# my_float1 = 1e-15  # 1 * (10 ** -15)
# print(my_float1)


# res = 0.1 + 0.2
#
# print(res)

# bool

# val = 0.1 + 0.2  # != 0.3
#
# res = val == 0.3
# print(res)
# print(type(res))
#
# my_bool1 = True
# my_bool2 = False
#
# res = val == 0.3
# print(res)
#
# res = val != 0.3
# print(res)
#
# res = val > 0.3
# print(res)
#
# res = val < 0.3
# print(res)
#
# res = val <= 0.3
# print(res)
#
# res = val >= 0.3
# print(res)
#
#
# print(round(1.3456, 2))
#
#
# # None
#
# my_none = None

# str

# my_str = "abcdef12345678"
# print(type(my_str))
# print(my_str)
#
# my_str = 'abcdef12345678jkgaydfhieytgiobkdugboidtbhoivuhtb' \
#          'ogidthiuevygiuehrboskvuhgobiurehsovidptiuhbpuvrkhdbgvhdtkghiouhioudhtbgophtbpgoh'
# print(type(my_str))
# print(my_str)
#
# my_str = ' '
# print(type(my_str))
# print(my_str)
#
# my_str = ''
# print(type(my_str))
# print(my_str)
#
#
# my_str = """kjgsdbiuydsbh
# kjgsvfiucgd
# jgsdvfcjg
# jgsdfjic
# """
# print(type(my_str))
# print(my_str)
#
# my_str = '''kjgsdbiuydsbh
# kjgsvfiucgd
# jgsdvfcjg
# jgsdfjic
# '''
# print(type(my_str))
# print(my_str)
#
#
# my_str = 'abcdef\'12"345678'
# print(type(my_str))
# print(my_str)
#
# my_str = 'abcdef\n12"345678\\'
# print(type(my_str))
# print(my_str)
#


my_str1 = 'abcdef'
my_str2 = '123456'

result = my_str1 + my_str2

print(result)
print(type(result))

result = my_str1 * 3
print(result)
print(type(result))

# result = my_str1 + 3
# print(result)
# print(type(result))

result = my_str1 == my_str2
print(result)

result = 'my_str' == 'ym_str'
print(result)

result = 'my_str' > 'my_str'
print(result)

print(ord('m'))


# task1 text

# task2 text
