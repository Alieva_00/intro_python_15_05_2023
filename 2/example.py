# my_int = 10
#
# my_float = 10.2
#
# my_int = int(my_float)
# print(my_int)
#
# my_int = int(10)
# print(my_int)
#
# my_float = float(my_int)
# print(my_float)
#
#
# my_str = '1234'
# print(type(my_str))
#
# my_int = int(my_str)
#
# print(my_int)
# print(type(my_int))
#
#
# # my_str = '1234asf'
# # print(type(my_str))
# #
# # my_int = int(my_str)
# #
# # print(my_int)
# # print(type(my_int))
#
# my_str = '1234'
# print(type(my_str))
#
# my_float = float(my_str)
#
# print(my_float)
# print(type(my_float))
#
#
# my_int = 1234
# print(type(my_int))
#
# my_str = str(my_int)
#
# print(my_str)
# print(type(my_str))
#
#
# my_float = 1234.8
# print(type(my_float))
#
# my_str = str(my_float)
#
# print(my_str)
# print(type(my_str))


# # bool
#
# my_bool = bool(0.0000000000001)
# print(my_bool)
#
# my_bool = bool('')
# print(my_bool)
#
# my_bool = bool(None)
# print(my_bool)

#
# my_str = 'qwertyuiop'
#
# result = 'qw' in my_str
# print(result)
#
#
# result = len(my_str)
# print(result)
#
# name = 'Artem'
# age = 39
#
# # Hi! My name is Artem, I'm 39 old.
#
# result_str = 'Hi! My name is ' + name + ', I\'m ' + str(age) + ' old.'
# print(result_str)
#
# result_str = 'Hi! My name is %s, I\'m %s old.' % (name, age)
# print(result_str)
#
# result_str = 'Hi! My name is {}, I\'m {} old.'.format(name, age)
# print(result_str)
#
# template = 'Hi! My name is {}, I\'m {} old.'
# result_str = template.format(name, age)
# print(result_str)
#
# name_value = 'Artem'
# age_value = 39
#
# template = 'Hi! My name is {name}, I\'m {age} old.'
# result_str = template.format(age=age_value, name=name_value)
# print(result_str)
#
# result_str = f'Hi! My name is {name_value}, I\'m {age_value} old.'
# print(result_str)


# string methods

#
# template = 'Hi! My name is Artem, I\'m 39 old.'
#
# result = template.replace('am', '++')
# print(result)
#
# result = template.replace('am', '')
# print(result)
#
# template = '___Hi! My name is Artem, I\'m 39 old.___'
#
# result = template.strip('_')
# print(result)
#
# result = template.lstrip('_')
# print(result)
#
# result = template.rstrip('_')
# print(result)
#
# result = template.upper()
# print(result)
#
# result = template.lower()
# print(result)
#
# result = template.title()
# print(result)
#
# template = 'hi! My name is Artem, I\'m 39 old.___'
# result = template.capitalize()
# print(result)
#
# template = 'Hi! My name is Artem, I\'m 39 old.___'
# result = template.count('am')
# print(result)
#
# template = 'Hi! My name is Artem, I\'m 39 old.'
# result = template.startswith('Hi')
# print(result)
#
# template = 'Hi! My name is Artem, I\'m 39 old.'
# result = template.endswith('.')
# print(result)
#
# template = '39.2'
# result = template.isdigit()
# print(result)
#
# template = 'Ajfcvsdiuhvb'
# result = template.isalpha()
# print(result)
#
# template = 'Ajfcvsdiuhvb 123'
# result = template.isalnum()
# print(result)

# template = 'abcdefghijklmn'
# result = template.find('cd')
# print(result)
#
# template = '0123456789'
# result = template.find('5')
# print(result)

# template = '0123456789'
# template = 'abcdefghijklmn'
# result = template[0]
# print(result)
#
# # result = template[1000]
# # print(result)
#
# result = template[13]
# print(result)
#
# result = template[len(template) - 1]
# print(result)
#
# template = '0123456789'
# result = template[-3]
# print(result)

# slice
# template = '0123456789'
# result = template[1:5]
# print(result)
# print(len(result))
#
# result = template[:5]  # [0:5]
# print(result)
#
# result = template[3:]  # [3:len(template)-1]
# print(result)
#
# result = template[3:len(template)]
# print(result)
#
# result = template[:]  # [0:9]
# print(result)
#
# result = template[-3:]  # [7:9]
# print(result)
#
# result = template[-8:-2]  # [2:7]
# print(result)
#
# result = template[1:8:3]
# print(result)
#
# result = template[9:1:-2]
# print(result)
#
# result = template[len(template)//2:]
# print(result)
# result = template[:len(template)//2]
# print(result)


# condition = None
#
# if condition:  # bool(condition)
#     print('condition is True')
#     print('condition is True')
#     print('condition is True')
# else:
#     print('condition is False')
#     print('condition is False')
#     print('condition is False')


# first = 10
# second = 20
#
# if first > second:
#     print('condition is first > second')
# else:
#     print('condition is first < second')
#     if first > 5:
#         print('condition is first > 5')
#     else:
#         print('condition is first < 5')


# first = 10
# second = 20
#
# if first > second:
#     print('condition is first > second')
# elif first == second:
#     print('condition is first == second')
# elif first > 10:
#     print('condition is first > 10')
# else:
#     print('condition is first < second')


# first = 10
# second = 20
#
# if first > second:
#     print('condition is first > second')
#
# if first == second:
#     print('condition is first == second')
# elif first > second:
#     print('condition is first > second')


userinput_ = input('Enter something here: ')
print(type(userinput_))

print(f'---> {userinput_}')
